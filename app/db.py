"""..."""
import psycopg2


def init_db_connection():
    """Initialize database connection"""
  #Establishing the connection.
    conn = psycopg2.connect(
      database="postgres",
      user="postgres",
      password="postgres",
      host="127.0.0.1",
      port="5432"
    )

    conn.autocommit = True
    return conn
