"""Utilility module"""
import random
import string


def generate_random_string(size=8):
    """Generate random strings of any length"""
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(size))
