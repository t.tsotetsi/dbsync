"""..."""
from fastapi import APIRouter, FastAPI

from app.db import init_db_connection
from app.utils import generate_random_string


router = APIRouter()

app = FastAPI()


@app.get("/users/", tags=["users"])
async def read_users():
    """Show all system users."""
    return [
        {"test_1": "Mickey"},
        {"test_2": "Mouse"}
    ]


@app.post("/dbs/{user_id}", tags=["databases"])
async def dbs(user_id):
    """Create database for the user"""
    conn = init_db_connection()

    cursor = conn.cursor()
    db_name = generate_random_string()
    db_username = generate_random_string(5)
    db_password = generate_random_string(5)

    sql = f"CREATE role {db_username} login password '{db_password}'"
    cursor.execute(sql)

    sql = f"CREATE database {db_name}"
    cursor.execute(sql)

    sql = f"GRANT ALL PRIVILEGES ON DATABASE {db_name} TO {db_username}"
    cursor.execute(sql)
    
    conn.close()

    return {
        "user_id": user_id,
        "db_name": db_name,
        "username": db_username,
        "password": db_password
    }
