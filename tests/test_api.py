from fastapi.testclient import TestClient

from app.api import app

client = TestClient(app)


def test_read_users():
    """Test read users endpoint"""
    resp = client.get("/users")
    assert resp.status_code == 200
    assert resp.json() == [
        {"test_1": "Mickey"},
        {"test_2": "Mouse"}
    ]
